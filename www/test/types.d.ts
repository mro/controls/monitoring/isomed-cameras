
import {
  Wrapper as tuWrapper,
  WrapperArray as tuWrapperArray } from '@vue/test-utils';

export = Tests
export as namespace Tests

declare namespace Tests {
  type Wrapper<T extends Vue = any> = tuWrapper<T>;
  type WrapperArray<T extends Vue = any> = tuWrapperArray<T>;
}
