// @ts-check

const
  { forEach, set, transform } = require('lodash'),
  url = require('url'),
  { readFileSync } = require('fs'),
  path = require('path'),

  Server = require('../../src/Server'),
  MJPEGServer = require('../../test/utils/MJPEGServer'),
  { camApp, stubs } = require('../../src/config-stub');

/**
 * @param  {any} env
 */
async function createApp(env) {
  env.sampleJPEG =
    readFileSync(path.join(__dirname, '..', 'dist', 'img', 'favicon.jpg'));

  // starting the MJPEG-Server Stub
  env.mjpegServer = new MJPEGServer(stubs);
  await env.mjpegServer.listen();

  // update camera config with target server port
  /** @type {number} */
  const port = env.mjpegServer.address().port;
  const groups = transform(camApp.groups,
    /**
     * @param {{[page: string]: {
     *  description?: string,
     *  cameras: Array<AppServer.CamApp.Camera>
     * }}} ret
     * @param {{description?: string,
     *  cameras: Array<AppServer.CamApp.Camera>}} group
     * @param {string} page
     */
    (ret, group, page) => {
      set(ret, [ page, 'cameras' ], []);
      forEach(group.cameras, (cam) => {
        ret[page].description = group?.description;
        ret[page].cameras.push({
          name: cam.name,
          url: `http://localhost:${port}` + url.parse(cam.url, true).path || ''
        });
      });
    }, {});

  env.server = new Server({
    title: "", port: 0, basePath: "",
    camApp: { groups, credentials: camApp.credentials }
  });

  return env.server.listen();
}

module.exports = { createApp };
