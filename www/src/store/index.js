// @ts-check

import { assign, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';
import CAMSource from "./sources/CAMSource";
import CAMModule from "./modules/cameras";

Vue.use(Vuex);

merge(storeOptions,
  /** @type {V.StoreOptions<AppStore.State>} */({
  /* NOTE: declare your store and modules here */
    state: {
      user: null,
      loading: false,
      page: null
    },
    mutations: {
      update: assign
    },
    getters: {
      isOperator(state) { return (state?.user?.cern_roles ?? []).includes("operator"); },
      isExpert(state) { return (state?.user?.cern_roles ?? []).includes("expert"); }
    },
    modules: {
      cameras: CAMModule
    }
  })
);

const store = createStore();

export default store;

export const sources = merge(baseSources, {
  cam: new CAMSource(store)
});

store.sources = sources;
