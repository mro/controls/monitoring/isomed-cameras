// @ts-check

import { assign } from "lodash";

/** @type {V.Module<AppStore.CamerasData>} */
export default {
  namespaced: true,
  state: () => ({
    cameras: {}
  }),
  mutations: {
    update: assign
  }
};
