// @ts-check

import './public-path';

import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router';
// @ts-ignore
import App from './App.vue';
import router from './router';
import BaseVue from '@cern/base-vue';

Vue.use(VueRouter);
Vue.use(BaseVue, { auth: true });

export default new Vue({
  el: '#app',
  components: { App },
  store,
  router,
  render: (h) => h(App)
});
