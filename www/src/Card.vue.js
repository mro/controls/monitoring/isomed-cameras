// @ts-check

import Vue from "vue";
import { mapState } from "vuex";
import {
  BaseCard,
  BaseKeyboardEventMixin as KBMixin } from "@cern/base-vue";
import InfoAlert from "./InfoAlert.vue";

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<ReturnType<KBMixin>> } Instance
 */

const component = Vue.extend({
  name: "Card",
  components: { InfoAlert },
  extends: BaseCard,
  mixins: [ KBMixin({ local: true }) ],
  data: () => {
    return {
      isSelected: false,
      isFocused: false,
      showInfo: false
    };
  },
  computed: {
    .../** @type {{ showKeyHints(): boolean }} */(mapState("ui", [ "showKeyHints" ]))
  },
  watch: {
    /**
     * @this {Instance}
     * @param {boolean} val
     */
    isSelected(val) {
      this.$emit("selected", val);
    }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey("ctrl-enter", () => {
      if (this.isFocused) { this.isSelected = true; }
    });
    this.onKey("esc", () => {
      if (this.isFocused) { this.isSelected = false; }
    });
    this.onKey("ctrl-h-keydown", (/** @type {Event} */e) => {
      if (this.isFocused) {
        e.preventDefault();
        this.showInfo = !this.showInfo;
      }
    });
  },
  methods: {
    /**
     * @this {Instance}
     * @param {string} name
     */
    hasSlot(name) {
      return !!this.$slots[name] || !!this.$scopedSlots[name];
    },
    /** @this {Instance} */
    toggleSelected() {
      this.isSelected = !this.isSelected;
    }
  }
});

export default component;
