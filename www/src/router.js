// @ts-check
import Vue from 'vue';
import Router from 'vue-router';

import Home from './components/Home.vue';
import CameraGroup from './components/CameraGroup.vue';
import UserInfo from "./components/UserInfo.vue";

import store from './store';

Vue.use(Router);

const router = new Router({
  routes: [
    { name: 'Home', path: '/', component: Home, meta: { navbar: false } },
    { path: '/index.html', redirect: '/', meta: { navbar: false } },
    { path: '/Layout', redirect: '/', meta: { navbar: false } },
    { name: "UserInfo", path: "/me", component: UserInfo,
      meta: { navbar: false } },

    { name: 'CameraGroup', path: '/:page', component: CameraGroup,
      meta: { navbar: false } }
  ]
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit("update", { page: to?.params?.page });
});

export default router;
