// @ts-check
const
  express = require('express'),
  helmet = require('helmet'),
  serveStatic = require('express-static-gzip'),
  path = require('path'),
  { attempt, noop } = require('lodash'),
  { makeDeferred } = require('@cern/prom'),
  ews = require('express-ws'),

  { CamService } = require('@cern/cam-express'),
  logger = require('./httpLogger'),
  auth = require('./auth');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this.camService = null;
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (process.env.NODE_ENV === 'production') {
      this.app.use(helmet.contentSecurityPolicy({
        useDefaults: true,
        directives: {
          imgSrc: [ "'self'", "data:", "blob:" ]
        }
      }));
    }
    logger.register(this.app);
    ews(this.app);

    this.router = express.Router();
    const dist = path.join(__dirname, '..', 'www', 'dist');
    this.router.use('/dist', serveStatic(dist, { enableBrotli: true }));

    /* everything after this point is authenticated */
    if (config.auth) {
      await auth.register(this.router, config);
      this.router.use(this.runAuth.bind(this, 'user'));
    }

    this.app.set('view engine', 'pug');
    this.app.set('views', path.join(__dirname, 'views'));

    this.router.get('/', (req, res) => res.render('index', config));

    if (config.auth) {
      // protected admin path for admin role only
      this.router.use('/admin', this.runAuth.bind(this, [ 'admin' ]));
    }

    /* NOTE: declare your additional endpoints here */
    const swaggerOpts = {
      definition: {
        info: {
          title: config.title,
          description: 'Web App API to interact with the back-end',
          contact: config.contact
        },
        basePath: config.basePath
      }
    };
    this.camService = new CamService(config.camApp, swaggerOpts);

    if (config.auth) {
      this.router?.put("/*",
        auth.roleCheck.bind(null, [ "expert", "operator" ]));
    }

    this.camService.register(this.router);

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render('error', config);
    });
  }

  close() {
    if (this.camService) {
      this.camService.release();
      this.camService = null;
    }
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {string|string[]} role
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  runAuth(role, req, res, next) {
    if (req.path.startsWith('/dist')) {
      return next();
    }
    return auth.roleCheck(role, req, res, next);
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
