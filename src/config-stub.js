// @ts-check
console.warn("Loading stub configuration");

module.exports = {
  title: "Isomed Cameras",
  basePath: "",
  beta: true,
  auth: {
    clientID: "base-website-template",
    clientSecret: "fce99307-a575-4fad-bff3-57214e569351",
    callbackURL: "http://localhost:8080/auth/callback",
    logoutURL: "http://localhost:8080"
  },
  camApp: {
    groups: {
      'page1': {
        description: "Cameras page 1",
        cameras: [
          { name: "cam1", url: "/mjpg/cam1.mjpg",
            ptz: { type: "PTZCam", url: "/ptz" } },
          { name: 'cam2', url: '/mjpg/cam2.mjpg' }
        ]
      },
      'page2': {
        description: "Cameras page 2",
        cameras: [
          { name: 'cam3', url: '/mjpg/cam3.mjpg',
            ptz: { type: "PTZCam", url: "/ptz" } },
          { name: 'cam4', url: '/mjpg/cam4.mjpg' }
        ]
      }
    },
    credentials: {
      'cam1': { user: 'USER1', password: 'PASS1' },
      'cam2': { user: 'USER2', password: 'PASS2' },
      'cam3': { user: 'USER3', password: 'PASS3' },
      'cam4': { user: 'USER4', password: 'PASS4' }
    }
  },
  // MJPEG Camera stubs
  stubs: {
    'cam1': {
      path: '/mjpg/cam1.mjpg',
      auth: { user: 'USER1', password: 'PASS1' }
    },
    'cam2': {
      path: '/mjpg/cam2.mjpg',
      auth: { user: 'USER2', password: 'PASS2' }
    },
    'cam3': {
      path: '/mjpg/cam3.mjpg',
      auth: { user: 'USER3', password: 'PASS3' }
    },
    'cam4': {
      path: '/mjpg/cam4.mjpg',
      auth: { user: 'USER4', password: 'PASS4' }
    }
  }
};
