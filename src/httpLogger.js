// @ts-check
const
  _ = require('lodash'),
  morgan = require('morgan'),
  debug = require('debug')('http');

/**
 * @typedef {import('express').IRouter} IRouter
 */

const stream = {
  write: (/** @type {string} */ message) => debug(_.trim(message))
};

module.exports = {
  /**
   * @param {IRouter} app
   */
  register(app) {
    if (debug.enabled) {
      debug('http logger enabled');
      app.use(morgan(
        /* eslint-disable-next-line max-len */
        ':method :url :status :response-time ms - :res[content-length] bytes',
        { stream, skip: (req) => _.endsWith(req.path, '.websocket') }));

      app.use(morgan('WS :url',
        { stream, immediate: true,
          skip: (req) => !_.endsWith(req.path, '.websocket') }));
    }
  }
};
