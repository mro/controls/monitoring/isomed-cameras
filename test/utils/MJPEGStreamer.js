// @ts-check

const
  { EventEmitter } = require('events'),
  basicAuth = require('basic-auth'),
  { isNil, forEach } = require('lodash');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').IRouter} IRouter
 *
 * @typedef {{
 *  auth?: { user: string, password: string }
 *  path: string
 * }} Options
 */

/**
 * @extends EventEmitter
 * @emits :
 *  - 'subscribed' when a new client subscribes
 *  - 'unsubscribed' when a client unsubscribes
 */
class MJPEGStreamer extends EventEmitter {

  /**
   * @param {Options} options
   */
  constructor(options) {
    super();
    this._id = 0;
    /** @type {{[id: number]: Response}} */
    this._clients = {};

    //  basic authentication
    this.user = options?.auth?.user;
    this.pass = options?.auth?.password;

    this.path = options?.path ?? '/video.mjpg';
    this.boundary = 'myboundary';

    this.unsubscribe = this.unsubscribe.bind(this);
  }

  /**
   * @param {IRouter} router
   */
  register(router) {
    this.release();

    router.get(this.path, (req, res, next) => {
      req.on('error', next);

      const auth = basicAuth(req);

      if (this.user && this.pass) {
        if (isNil(auth)) {
          res.sendStatus(401);
        }
        else if (!this.checkCreds(auth.name, auth.pass)) {
          res.sendStatus(403);
        }
      }

      res
      .status(200)
      .set('Cache-Control', 'no-cache')
      .set('Connection', 'close')
      .set('Content-Type',
        `multipart/x-mixed-replace; boundary=${this.boundary}`)
      .set('Pragma', 'no-cache')
      .flushHeaders();

      const id = this.subscribe(res);
      res.once('close', this.unsubscribe.bind(id));
    });
  }

  /**
   * @param {Response} response
   * @return {number}
   */
  subscribe(response) {
    const id = this._id++;
    this._clients[id] = response;
    this.emit('subscribed', id);
    return id;
  }

  /**
   * @param {number} id
   */
  unsubscribe(id) {
    const res = this._clients[id];
    if (res) {
      res.removeListener('close', this.unsubscribe.bind(id));
      res.req?.removeAllListeners('error');
    }
    delete this._clients[id];
    this.emit('unsubscribed', id);
  }

  /**
   *  @param {Buffer} jpeg
   *  @param {boolean} [contentLength=true]
   */
  sendFrame(jpeg, contentLength = true) {
    forEach(this._clients, (res) => {
      res.write(`--${this.boundary}\r\n`);
      res.write("Content-Type: image/jpeg\r\n");
      if (contentLength) { res.write(`Content-Length: ${jpeg.length}\r\n`); }
      res.write("\r\n");
      res.write(jpeg, 'binary');
      res.write("\r\n");
    });
  }

  /**
   *  @param {Buffer} data
   */
  sendRaw(data) {
    forEach(this._clients, (res) => { res.write(data, 'binary'); });
  }

  /**
   * @param {Buffer} [jpeg] - last JPEG frame
   */
  stop(jpeg) {
    forEach(this._clients, (res) => res.end(jpeg));
    this._clients = {};
  }

  /**
   * @param {string} user
   * @param {string} pass
   * @return {boolean}
   */
  checkCreds(user, pass) {
    if (this.user === user && this.pass === pass) {
      return true;
    }
    return false;
  }

  release() {
    this.stop();
    this._id = 0;
  }
}

module.exports = MJPEGStreamer;

