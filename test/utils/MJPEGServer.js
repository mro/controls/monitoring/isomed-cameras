// @ts-check

const
  express = require('express'),
  { makeDeferred } = require('@cern/prom'),

  MJPEGStreamer = require('./MJPEGStreamer'),
  { forEach, mapValues } = require('lodash');

/**
 * @typedef {import('net').AddressInfo} AddressInfo
 * @typedef {{ [name: string]: {
 *    auth?: { user: string, password: string }
 *    path: string
 * } }} Config
 */

class MJPEGServer {
  /**
   * @param {Config} config
   * @param {number} [port]
   */
  constructor(config, port) {
    this.port = port ?? 0;
    this.config = config;
    this.app = express();

    /** @type {{[name: string]: MJPEGStreamer}} */
    this.streamers = mapValues(config, (cam) => {
      const s = new MJPEGStreamer({ auth: cam.auth, path: cam.path });
      s.register(this.app);
      return s;
    });
  }

  close() {
    forEach(this.streamers, (s) => {
      s.release();
    });

    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @return {Promise<void>}
   */
  listen() {
    const def = makeDeferred();
    var server = this.app.listen(this.port, () => {
      this.server = server;
      const port =
        this.port || /** @type {AddressInfo} */(server.address()).port;
      console.log(`MJPEG Server Stub listening on http://localhost:${port}`);
      def.resolve(undefined);
    });
    return def.promise;
  }

  /**
   * @return {AddressInfo | string | null}
   */
  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = MJPEGServer;
