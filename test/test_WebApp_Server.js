'use strict';

const
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { toString } = require('lodash'),
  sa = require('superagent'),

  AppServer = require('../src');

describe('WebApp Server', function() {
  let env = {};

  beforeEach(async function() {
    await AppServer.listen();
    env.addr = AppServer.address();
  });

  afterEach(function() {
    AppServer.close();
    env = {};
  });

  it(`serves resources in '/dist' without any authentication`, function() {
    return sa.get(`http://localhost:${env.addr.port}/dist/img/favicon.svg`)
    .then((res) => {
      expect(res.statusCode).to.be.equal(200);
      expect(toString(res.body)).to.include('</svg>');
    });
  });
});
